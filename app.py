from flask import Flask
import time

app = Flask(__name__)



@app.route("/")
def info():
  ts = time.time()

  #return "<h1>First version of the Python app</h1>"
  return "<h1>Second version of the Python app</h1><p>Current time: " + str(ts) + "</p>"

if __name__ == "__main__":
  app.run(host="0.0.0.0", port=7070)
