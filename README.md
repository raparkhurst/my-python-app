# Python Flask application

- This application starts web server using Flask package
- Web server responds to the requests from the clients to the root URL

## Application versions
 
- v1.0.1 - Initial version
- v1.0.2 - Second version
